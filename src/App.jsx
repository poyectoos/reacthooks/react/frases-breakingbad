import { useState, useEffect } from 'react';
import styled from '@emotion/styled';

import logo from './logo.svg';
import Frase from './components/Frase';

const Button = styled.button`
  background: #0f574e;  /* fallback for old browsers */
  background: -webkit-linear-gradient(to right, #007d35, #0f574e);  /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to right, #007d35, #0f574e);
  font-family: Arial, Helvetica, sans-serif;
  margin-top: 2rem;
  padding: 1rem 3rem;
  font-size: 2rem;
  border: 2px solid black;
  color: #FFFFFF;
  transition: background .8s ease;
  

  &:hover {
    cursor: pointer;
    background: #116358;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #008A39, #0f574e);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #008A39, #116358);
  }
`;
const Contenedor = styled.div`
  display: flex;
  align-items: center;
  padding-top: 2rem;
  flex-direction: column;
`;

function App() {

  // State para la frase
  const [ frase, actualizarFrase ] = useState({});

  useEffect(() => {
    consultar();
  }, []);

  // Funcion que consulta el api de braking bad
  const consultar = async () => {
    actualizarFrase({});
    try {
      const resultado = await fetch('https://breaking-bad-quotes.herokuapp.com/v1/quotes');
      const data = await resultado.json();
      actualizarFrase(data[0]);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <Contenedor>
      <img src={logo} className="app-logo" alt="logo" />
      <Button
        onClick={consultar}
      >
        Tell me something
      </Button>
      <Frase
        frase={frase}
      />
    </Contenedor>
  );
}

export default App;
