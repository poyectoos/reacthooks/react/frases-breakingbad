import styled from '@emotion/styled';
import PropTypes from 'prop-types';

import Spinner from './Spinner';

const Div = styled.div`
  padding: 1rem;
  border-radius: 0.5rem;
  background-color: #FFFFFF;
  max-width: 800px;
  margin-top: 30px;

  h1 {
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    position: relative;
    padding-left: 4rem;
    padding-right: 4rem;

    &::before {
      content: open-quote;
      font-size: 9rem;
      color: black;
      position: absolute;
      left: -1rem;
      top: -1.5rem;
    }
    &::after {
      content: close-quote;
      font-size: 9rem;
      color: black;
      position: absolute;
      right: -1rem;
      top: -1.5rem;
    }
  }

  h4 {
    font-family: Verdana, Geneva, Tahoma, sans-serif;
    font-size: 1.4rem;
    font-weight: bold;
    text-align: right;
    color: #666666;
    margin-top: 1rem;
  }
`;

const Frase = ({ frase }) => {
  if (Object.keys(frase).length === 0) return <Spinner />;

  const { quote, author } = frase;

  return (
    <Div>
      <h1>{ quote }</h1>
      <h4>- { author }</h4>
    </Div>
  );
};

Frase.propTypes = {
  frase: PropTypes.object.isRequired
};

export default Frase;